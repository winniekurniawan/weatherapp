package com.example.bmkgcuaca.Helper;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Helper
{
    static String stream = null;
    private int code = 0;

    public Helper ()
    {

    }

    public String getHTTPData (String urlString)
    {
        try {
            URL url = new URL(urlString);
            HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();

            Log.d("code", String.valueOf(httpUrlConnection.getResponseCode()));
            code = httpUrlConnection.getResponseCode();

            if (httpUrlConnection.getResponseCode() == 200)
            {
                BufferedReader br = new BufferedReader(new InputStreamReader(httpUrlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;

                while((line = br.readLine()) != null)
                {
                    sb.append(line);
                }

                stream = sb.toString();
                httpUrlConnection.disconnect();
            }
            else
            {
                stream = "city not found";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream;
    }

    public int getCode() {
        return code;
    }
}
