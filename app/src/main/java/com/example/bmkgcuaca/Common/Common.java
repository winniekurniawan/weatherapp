package com.example.bmkgcuaca.Common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Common
{
    public static String API_KEY = "92f80f5ab5d35b1917421f334af723a8";
    public static String API_LINK = "http://api.openweathermap.org/data/2.5/weather";

    public static String apiRequest (String lat, String lon)
    {
        StringBuilder sb = new StringBuilder(API_LINK);
        sb.append(String.format("?lat=%s&lon=%s&APPID=%s&units=metric", lat, lon, API_KEY));
        return sb.toString();
    }

    public static String apiRequestFahrenheit (String lat, String lon)
    {
        StringBuilder sb = new StringBuilder(API_LINK);
        sb.append(String.format("?lat=%s&lon=%s&APPID=%s", lat, lon, API_KEY));
        return sb.toString();
    }

    public static String apiRequestByCity (String city)
    {
        StringBuilder sb = new StringBuilder(API_LINK);
        sb.append(String.format("?q=%s&APPID=%s&units=metric", city, API_KEY));
        return sb.toString();
    }

    public static String apiRequestByCityFahrenheit (String city)
    {
        StringBuilder sb = new StringBuilder(API_LINK);
        sb.append(String.format("?q=%s&APPID=%s", city, API_KEY));
        return sb.toString();
    }

    public static String unixTimeStampToDateTime (double unixTimeStamp)
    {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        date.setTime( (long)unixTimeStamp*1000 );
        return dateFormat.format(date);
    }

    public static String getImage (String icon)
    {
        return String.format("http://openweathermap.org/img/wn/%s@2x.png", icon);
    }

    public static String getDateNow ()
    {
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
