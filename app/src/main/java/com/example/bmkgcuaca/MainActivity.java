package com.example.bmkgcuaca;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bmkgcuaca.Common.Common;
import com.example.bmkgcuaca.Helper.Helper;
import com.example.bmkgcuaca.Model.OpenWeatherMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import java.lang.reflect.Type;

public class MainActivity extends AppCompatActivity implements LocationListener, View.OnClickListener {

    private TextView cityTextView;
    private TextView lastUpdateTextView;
    private TextView tempTextView;
    private TextView satuanTextView;
    private ImageView iconImageView;
    private TextView descriptionTextView;
    private TextView humidityTextView;
    private TextView timeTextView;

    private RelativeLayout llBackground;

    private EditText cityEditText;
    private Button btnSearch;
    private Button btnCancel;

    private LocationManager locationManager;
    String provider;
    static double lat, lon;
    OpenWeatherMap openWeatherMap = new OpenWeatherMap();
    private Context con;

    int MY_PERMISSION = 0;
    private boolean celciusFahrenheit = false; //false= celcius, true = fahreinheit
    private boolean city = false; //false=by location, true=by city
    private int code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.llBackground = findViewById(R.id.llBackground);

        this.cityTextView = findViewById(R.id.cityTextView);
        this.lastUpdateTextView = findViewById(R.id.lastUpdateTextView);
        this.tempTextView = findViewById(R.id.tempTextView);
        this.satuanTextView = findViewById(R.id.satuanTempTextView);
        this.iconImageView = findViewById(R.id.iconImageView);
        this.descriptionTextView = findViewById(R.id.descriptionTextView);
        this.humidityTextView = findViewById(R.id.humidityTextView);
        this.timeTextView = findViewById(R.id.timeTextView);

        this.cityEditText = findViewById(R.id.cityEditText);
        this.btnCancel = findViewById(R.id.btnCancel);
        this.btnSearch = findViewById(R.id.btnSearch);

        this.satuanTextView.setOnClickListener(this);
        this.btnSearch.setOnClickListener(this);
        this.btnCancel.setOnClickListener(this);


        //get coordinates
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        provider = locationManager.getBestProvider(new Criteria(), false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.INTERNET,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.SYSTEM_ALERT_WINDOW,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, MY_PERMISSION);
        }
        Location location = locationManager.getLastKnownLocation(provider);

        if (location == null) {
            Log.e("TAG", "NO LOCATION");
        }

        Toast.makeText(getApplicationContext(),"Please turn on your location",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.INTERNET,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.SYSTEM_ALERT_WINDOW,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, MY_PERMISSION);
        }

        locationManager.removeUpdates(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.INTERNET,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.SYSTEM_ALERT_WINDOW,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, MY_PERMISSION);
        }
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (city == false) {
            lat = location.getLatitude();
            lon = location.getLongitude();

            if (celciusFahrenheit == false) {
                new GetWeather().execute(Common.apiRequest(String.valueOf(lat), String.valueOf(lon)));
            } else {
                new GetWeather().execute(Common.apiRequestFahrenheit(String.valueOf(lat), String.valueOf(lon)));
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onClick(View v) {
        String cityName = this.cityEditText.getText().toString();

        if (v == this.satuanTextView)
        {
            if (celciusFahrenheit == false)
            {
                celciusFahrenheit = true;
            }
            else
            {
                celciusFahrenheit = false;
            }

            if (this.city == false)
            {
                byLocation();
            }
            else
            {
                byCity(cityName);
            }
        }
        else if (v == this.btnSearch)
        {
            this.city = true;
            this.byCity(cityName);
        }
        else if (v == this.btnCancel)
        {
            this.city = false;
            this.cityEditText.setText("");
            byLocation();
        }
    }

    private void byLocation ()
    {
        if (celciusFahrenheit == false)
        {
            new GetWeather().execute(Common.apiRequest(String.valueOf(lat), String.valueOf(lon)));
        }
        else
        {
            new GetWeather().execute(Common.apiRequestFahrenheit(String.valueOf(lat), String.valueOf(lon)));
        }
    }

    private void byCity (String cityName)
    {
        if (celciusFahrenheit == false)
        {
            new GetWeather().execute(Common.apiRequestByCity(cityName));
        }
        else
        {
            new GetWeather().execute(Common.apiRequestByCityFahrenheit(cityName));
        }
    }

    private class GetWeather extends AsyncTask<String, Void, String>
    {
        ProgressDialog pd = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            pd.setTitle("Please wait...");
//            pd.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String stream = null;
            String urlString = strings[0];

            Helper http = new Helper();
            stream = http.getHTTPData(urlString);

            code = http.getCode();

            return stream;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (code != 200)
            {
                Toast.makeText(getApplicationContext(),"City not found",Toast.LENGTH_SHORT).show();
                return;
            }

            Gson gson = new Gson();
            Type mType = new TypeToken<OpenWeatherMap>(){}.getType();
            openWeatherMap = gson.fromJson(s, mType);
            pd.dismiss();

            String icon = openWeatherMap.getWeather().get(0).getIcon();

            cityTextView.setText(String.format("%s, %s", openWeatherMap.getName(), openWeatherMap.getSys().getCountry()));
            lastUpdateTextView.setText(String.format("Last Updated : %s", Common.getDateNow()));
            descriptionTextView.setText(String.format("%s", openWeatherMap.getWeather().get(0).getDescription()));
            humidityTextView.setText(String.format("Humidity : %d%%", openWeatherMap.getMain().getHumidity()));
            timeTextView.setText(String.format("Sunrise : %s / Sunset : %s", Common.unixTimeStampToDateTime(openWeatherMap.getSys().getSunrise()),
                    Common.unixTimeStampToDateTime(openWeatherMap.getSys().getSunset())));
            tempTextView.setText(String.format("%.2f", openWeatherMap.getMain().getTemp()));
            Picasso.get()
                    .load(Common.getImage(openWeatherMap.getWeather().get(0).getIcon()))
                    .into(iconImageView);

            if (celciusFahrenheit == false) //celcius
            {
                satuanTextView.setText((char) 0x00B0 + "C");
            }
            else
            {
                satuanTextView.setText((char) 0x00B0 + "F");
            }

            if (icon.contains("01")) // day/night
            {
                if (icon.contains("d"))
                {
                    llBackground.setBackgroundResource(R.drawable.day);
                }
                else
                {
                    llBackground.setBackgroundResource(R.drawable.night);
                }
            }
            else if (icon.contains("02") || icon.contains("03") || icon.contains("04")) //berawan
            {
                llBackground.setBackgroundResource(R.drawable.cloud);
            }
            else if (icon.contains("09") || icon.contains("10")) //hujan
            {
                llBackground.setBackgroundResource(R.drawable.rainy);
            }
            else if (icon.contains("11")) // thunder
            {
                llBackground.setBackgroundResource(R.drawable.thunder);
            }
            else if (icon.contains("13")) //snow
            {
                llBackground.setBackgroundResource(R.drawable.snow);
            }
            else if (icon.contains("50")) //mist
            {
                llBackground.setBackgroundResource(R.drawable.mist);
            }
        }
    }
}
